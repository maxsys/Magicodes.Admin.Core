﻿// ======================================================================
// 
//           Copyright (C) 2019-2020 湖南心莱信息科技有限公司
//           All rights reserved
// 
//           filename : TreeItemDataDto.cs
//           description :
// 
//           created by 雪雁 at  2019-06-14 11:22
//           开发文档: docs.xin-lai.com
//           公众号教程：magiccodes
//           QQ群：85318032（编程交流）
//           Blog：http://www.cnblogs.com/codelove/
//           Home：http://xin-lai.com
// 
// ======================================================================

namespace Magicodes.Admin.Application.Dto
{
    /// <summary>
    ///     Tree每一项数据类型定义
    /// </summary>
    public class TreeItemDataDto
    {
        /// <summary>
        ///     标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        ///     图标
        /// </summary>
        public string Icon { get; set; }
    }
}