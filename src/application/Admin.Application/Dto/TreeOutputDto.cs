﻿// ======================================================================
// 
//           Copyright (C) 2019-2020 湖南心莱信息科技有限公司
//           All rights reserved
// 
//           filename : TreeOutputDto.cs
//           description :
// 
//           created by 雪雁 at  2019-06-14 11:22
//           开发文档: docs.xin-lai.com
//           公众号教程：magiccodes
//           QQ群：85318032（编程交流）
//           Blog：http://www.cnblogs.com/codelove/
//           Home：http://xin-lai.com
// 
// ======================================================================

using System.Collections.Generic;

namespace Magicodes.Admin.Application.Dto
{
    /// <summary>
    ///     Tree Table输出结果
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class TreeTableOutputDto<TEntity> where TEntity : class
    {
        /// <summary>
        ///     数据集合
        /// </summary>
        public ICollection<TreeTableRowDto<TEntity>> Data { get; set; }
    }
}